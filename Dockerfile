FROM openjdk:12-alpine
WORKDIR ./APP
COPY ./build/libs/cars-api.jar ./APP/cars-api.jar
EXPOSE 5000
ENTRYPOINT ["java","-jar","./APP/cars-api.jar"]
